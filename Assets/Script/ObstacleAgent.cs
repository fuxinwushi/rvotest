
using RVO;
using UnityEngine;

public class ObstacleAgent
{
    /// <summary>
    /// 障碍物图形
    /// </summary>
    public ObstaclePolygon polygon;

    /// <summary>
    /// 障碍物物体
    /// </summary>
    public Transform tf;
}
